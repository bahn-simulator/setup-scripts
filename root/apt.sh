#!/bin/bash

sudo apt-get update
sudo apt-get remove --yes meson
sudo apt-get install --yes gcc-10 git curl python3-pip
sudo -H python3 -m pip install meson ninja
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install --yes libsdl2-dev libsdl2-image-dev libsdl2-gfx-dev libglm-dev libavcodec-dev libavdevice-dev libavfilter-dev libavformat-dev libavutil-dev libswresample-dev libswscale-dev libpostproc-dev git-lfs

if [ $1 ]
then
    git clone https://git.thm.de/bahn-simulator/simulator.git
else
    git clone --recursive https://git.thm.de/bahn-simulator/simulator.git
fi

cd simulator
meson setup build
meson compile -C build
meson test -C build
cd ..
