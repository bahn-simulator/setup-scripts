#!/bin/bash

command -v brew >/dev/null 2>&1 || { echo "homebrew not installed. Starting installation!"; /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"; }


brew install sdl2
brew install sdl2_image
brew install sdl2_gfx
brew install ffmpeg
brew install git-lfs
brew install python3
brew install meson

if [ $1 ]
then
    git clone https://git.thm.de/bahn-simulator/simulator.git
else
    git clone --recursive https://git.thm.de/bahn-simulator/simulator.git
fi

cd simulator
meson setup build
meson compile -C build
meson test -C build
cd ..
