#!/bin/bash

pacman -Sy --noconfirm --needed pactoys-git git fish
pacboy --noconfirm update
pacboy sync --needed --noconfirm \
    SDL2:c \
    SDL2_image:c \
    SDL2_gfx:c \
    ffmpeg:c \
    glm:c \
    git-lfs:c \
    meson:c \
    cmake:c \
    make:c \
    toolchain:c \
    ninja:c \
    glslang:c \
    python:c \
    python-pip:c
