#!/bin/bash

pacman -Sy --noconfirm --needed pactoys-git git fish
pacboy --noconfirm update
pacboy sync --needed --noconfirm \
    SDL2:x \
    SDL2_image:x \
    SDL2_gfx:x \
    ffmpeg:x \
    glm:x \
    git-lfs:x \
    meson:x \
    cmake:x \
    make:x \
    toolchain:x \
    ninja:x \
    glslang:x \
    python:x \
    python-pip:x
